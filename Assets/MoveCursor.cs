﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cube_move : MonoBehaviour {

    public float move_rate = 0.05f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.A))
            gameObject.transform.Translate(-move_rate, 0, 0);

        if (Input.GetKey(KeyCode.S))
            gameObject.transform.Translate(0, 0, -move_rate);

        if (Input.GetKey(KeyCode.D))
            gameObject.transform.Translate(move_rate, 0, 0);

        if (Input.GetKey(KeyCode.W))
            gameObject.transform.Translate(0, 0, move_rate);




    }
}
