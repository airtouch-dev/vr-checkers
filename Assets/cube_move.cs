﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCursor : MonoBehaviour {

    private float move_rate = 1;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.LeftArrow))
            gameObject.transform.Translate(-move_rate, 0, 0);

        if (Input.GetKeyDown(KeyCode.DownArrow))
            gameObject.transform.Translate(0, 0, -move_rate);

        if (Input.GetKeyDown(KeyCode.RightArrow))
            gameObject.transform.Translate(move_rate, 0, 0);

        if (Input.GetKeyDown(KeyCode.UpArrow))
            gameObject.transform.Translate(0, 0, move_rate);




    }
}
